<?php

namespace App\Console\Commands;

use App\Jobs\TestJob;
use Illuminate\Console\Command;

class CreateJobsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:jobs {amount}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create x jobs';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $amount = (int) $this->argument('amount');

        if (!is_numeric($amount)) {
            $this->error('Amount must be a number');
            return;
        }

        for ($i = 0; $i < $amount; $i++) {
            dispatch(new TestJob(500));
        }
    }
}
