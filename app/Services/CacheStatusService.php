<?php

namespace App\Services;

use App\Services\Concerns\TestRedis;
use Exception;
use Illuminate\Support\Facades\Cache;

class CacheStatusService extends AbstractStatusService
{
    use TestRedis;

    public function name(): string
    {
        return 'cache';
    }

    public function currentDefaultDriver(): string
    {
        return Cache::getDefaultDriver();
    }

    protected function accessTry(): string|Exception|null
    {
        return match ($driver = $this->currentDefaultDriver()) {
            'redis' => $this->redisAccessTry(),
            default => new Exception("Current cache driver is '$driver' and not Redis"),
        };
    }
}
