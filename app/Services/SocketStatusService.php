<?php

namespace App\Services;

use Exception;

class SocketStatusService extends AbstractStatusService
{
    public function name(): string
    {
        return 'socket';
    }

    public function currentDefaultDriver(): string
    {
        return 'fake-socket';
    }

    protected function accessTry(): string|Exception|null
    {
        if (exec('ping -c 1 fake-socket; echo $?') == 0) {
            return null;
        }

        return new Exception('Socket is not running');
    }
}
