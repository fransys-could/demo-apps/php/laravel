<?php

namespace App\Http\Controllers;

use App\Services\AbstractStatusService;
use Illuminate\Http\JsonResponse;

class StatusController extends Controller
{
    /** @var array<AbstractStatusService> */
    private array $statusServices;

    public function __construct(AbstractStatusService ...$statusServices) {
        $this->statusServices = $statusServices;
    }

    public function index() : JsonResponse
    {
        $output = [];

        foreach ($this->statusServices as $statusService) {
            $output[$statusService->name()] = [
                'driver' => $statusService->currentDefaultDriver(),
                'status' => $statusService->status(),
                'output' => $statusService->output(),
            ];
        }

        return response()->json($output);
    }
}
