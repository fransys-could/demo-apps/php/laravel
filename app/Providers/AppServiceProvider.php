<?php

namespace App\Providers;

use App\Http\Controllers\StatusController;
use App\Services\AbstractStatusService;
use App\Services\CacheStatusService;
use App\Services\DatabaseStatusService;
use App\Services\QueueStatusService;
use App\Services\SocketStatusService;
use App\Services\WorkerStatusService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->tag(
            [
                CacheStatusService::class,
                DatabaseStatusService::class,
                QueueStatusService::class,
                WorkerStatusService::class,
                SocketStatusService::class,
            ],
            'statusServices',
        );

        $this->app->when(StatusController::class)
            ->needs(AbstractStatusService::class)
            ->giveTagged('statusServices');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
